# vim:filetype=sh

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -la --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'

alias grep='grep --color=tty -d skip'
alias egrep='egrep --color=tty -d skip'

alias pc='ps -fC'
alias px='ps aux'
alias killz='kill -KILL'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias du='du -h'
alias dux='du -hx --max-depth=1'
alias free='free -m'                      # show sizes in MB
alias g='git'

alias mkae='make'

alias startx-laptop='CONFIGURATION=laptop-only startx'
alias startx-office='CONFIGURATION=work-office startx'
alias startx-conf='CONFIGURATION=work-conference startx'
alias startx-home='CONFIGURATION=home-office startx'
alias i3ws-left='i3-msg move workspace to output left'
alias i3ws-right='i3-msg move workspace to output right'

alias cent='cd /mnt/smb/clients/SEI/PASS/CENT'
alias sidc='cd /mnt/smb/clients/SEI/PASS/SIDC'

# sometimes useful
_foreach() {
    for i in `$1`; do $2 $i; done
}

alias foreach=_foreach

# coding helpers
ls_vala() {
    a=$1
    if [[ "$1" == */ ]]; then
        a=`echo "${1%?}"`
    fi
    wc -l $a/*.vala
}
alias lv=ls_vala

# find simplifiers

# @param search path
# @param case insensitive search name
# @param search term within file
_fsearch() {
    #find $1 -type f -iname $2 -exec grep -H $3 {} \;
    find $1 -type f -iname $2 | xargs egrep -e $3
}

# examples:
#
# 1. Replace a string in every file that contains it.
#
# for file in `fsearch . Makefile.am "--pkg\sdcs-config" | awk '{gsub(/:/,"\t"); print $1}'`
# do
#   sed -i 's/--pkg dcs-config/--pkg dcs-build/' $file
# done
#
alias fsearch=_fsearch

# search and replace, cwd is assumed
#
# @param file expression to match
# @param search expression
# @param string to replace with
_sar() {
    for file in `fsearch . $1 $2 | awk '{gsub(/:/,"\t"); print $1}' | uniq`
    do
        echo "Replacing $2 with $3 in $file"
        sed -i 's/'"$2"'/'"$3"'/g' "$file"
    done
}

# example
#
# sar
alias sar=_sar

# git
alias gistory='history | grep git'
alias git-graph='git log --pretty=format:"%h : %s" --graph'

# ninja
alias nibu='ninja -C _build'
alias nite='ninja -C _build test'

# remote access test
alias nmap-ssh='nmap -p 22 --open -sV'

rdp() {
    xfreerdp /size:$1 /u:$2 +clipboard /v:$3
}

alias rdpsm='rdp 1440x900'
alias rdplg='rdp 1920x1080'
