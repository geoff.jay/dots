--[[
Clock Rings by londonali1010 (2009) , mod by arpinux , remod by raininja

This script draws percentage meters as rings, and also draws clock hands if you want! It is fully customisable; all options are described in the script. This script is ba
sed off a combination of my clock.lua script and my rings.lua script.

IMPORTANT: if you are using the 'cpu' function, it will cause a segmentation fault if it tries to draw a ring straight away. The if statement on line 145 uses a delay to
make sure that this doesn't happen. It calculates the length of the delay by the number of updates since Conky started. Generally, a value of 5s is long enough, so if you
 update Conky every 1s, use update_num>5 in that if statement (the default). If you only update Conky every 2s, you should change it to update_num>3; conversely if you up
date Conky every 0.5s, you should use update_num>10. ALSO, if you change your Conky, is it best to use "killall conky; conky" to update it, otherwise the update_num will
not be reset and you will get an error.

To call this script in Conky, use the following (assuming that you save this script to ~/scripts/clock_rings.lua):
   lua_load ~/scripts/clock_rings.lua
   lua_draw_hook_pre clock_rings

Changelog:
+ v1.2 -- Heavily modified (01.11.2012)
+ v1.0 -- Original release (30.09.2009)
--]]

function network_interface()
	return conky_parse('${if_existing /proc/net/route enp0s25}enp0s25${else}${if_existing /proc/net/route wlo1}wlo1${endif}${endif}')
end

-- function network_uplimit()
--	return conky_parse('$(if
settings_table = {
 --   clock
  {--1
           name='time',
           arg='%I',
           max=12,
           bg_colour=0xffffff,
           bg_alpha=0.1,
           fg_colour=0xffa300,
           fg_alpha=0.6,
           x=824, y=400,
           radius=150,
           thickness=5,
           start_angle=0,
           end_angle=360
       },
       {--2
           name='time',
           arg='%M.%S',
           max=60,
           bg_colour=0xffffff,
           bg_alpha=0.1,
           fg_colour=0x3399cc,
           fg_alpha=0.4,
           x=824, y=400,
           radius=159,
           thickness=5,
           start_angle=0,
           end_angle=360
       },
       {--3
           name='time',
           arg='%S',
           max=60,
           bg_colour=0xffffff,
           bg_alpha=0.1,
           fg_colour=0x3399cc,
           fg_alpha=0.6,
           x=824, y=400,
           radius=164,
           thickness=5,
           start_angle=0,
           end_angle=360
       },
       {--4
           name='time',
           arg='%d',
           max=31,
           bg_colour=0xffffff,
           bg_alpha=0.1,
           fg_colour=0x3388cc,
           fg_alpha=0.8,
           x=824, y=400,
           radius=180,
           thickness=5,
           start_angle=-90,
           end_angle=90
   },
       {--5
           name='time',
           arg='%m',
           max=12,
           bg_colour=0xffffff,
           bg_alpha=0.1,
           fg_colour=0x3399cc,
           fg_alpha=1,
           x=824, y=400,
           radius=186,
           thickness=5,
           start_angle=-90,
           end_angle=90
   },
-- upper quadrant
   {--6
           name='swapperc',
           arg='',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x33ccff,
           fg_alpha=0.8,
           x=400, y=300,
           radius=130,
           thickness=10,
           start_angle=-40,
           end_angle=40
   },
   {--7
	   name='memperc',
           arg='',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x3399cc,
           fg_alpha=0.3,
           x=400, y=300,
           radius=155,
           thickness=40,
           start_angle=-40,
           end_angle=40
   },
-- lower quadrant
   {--8
           name='upspeedf',
           arg=network_interface(),
	   max=300,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0xff2222,
           fg_alpha=0.3,
           x=400, y=300,
           radius=100,
           thickness=25,
           start_angle=140,
           end_angle=220
   },
   {--9
           name='downspeedf',
           arg=network_interface(),
	   max=1000,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x331111,
           fg_alpha=0.3,
           x=400, y=300,
           radius=145,
           thickness=50,
           start_angle=140,
           end_angle=220
   },
-- right quadrant
   {--10
           name='cpu',
           arg='cpu0',
	   max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x66CC00,
	   fg_alpha=0.8,
           x=420,
	   y=300,
           radius=100,
           thickness=6,
           start_angle=40,
           end_angle=140
   },
   {--11
	   name='cpu',
           arg='cpu1',
	   max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
	   fg_colour=0x66CC00,
           fg_alpha=0.8,
           x=420,
	   y=300,
           radius=107,
           thickness=6,
           start_angle=40,
           end_angle=140
   },
   {--12
	   name='cpu',
           arg='cpu2',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x66CC00,
           fg_alpha=0.8,
           x=420,
           y=300,
           radius=114,
           thickness=6,
           start_angle=40,
           end_angle=140
   },
   {--13
           name='cpu',
           arg='cpu3',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x66CC00,
           fg_alpha=0.8,
           x=420,
           y=300,
           radius=121,
           thickness=6,
           start_angle=40,
           end_angle=140
   },
   {--14
           name='battery_percent',
           arg='BAT0',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x66CC00,
           fg_alpha=0.3,
           x=420,
           y=300,
           radius=156,
           thickness=40,
           start_angle=40,
           end_angle=140
    },
-- left quadrant
   {--15
           name='entropy_avail',
           arg='',
           max=10000,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x44cc44,
           fg_alpha=0.4,
           x=380, y=300,
           radius=106,
           thickness=2,
           start_angle=220,
           end_angle=320
   },
--   {--15a
--           name='entropy_perc',
--           arg='',
--           max=100,
--           bg_colour=0xffffff,
--           bg_alpha=0.2,
--           fg_colour=0x1acc44,
--           fg_alpha=0.4,
--           x=380, y=300,
--           radius=101,
--           thickness=3,
--           start_angle=220,
--           end_angle=320
--   },
   {--16
           name='fs_used_perc',
           arg='/',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x2210cc,
           fg_alpha=0.4,
           x=380, y=300,
           radius=124,
           thickness=20,
           start_angle=220,
           end_angle=320
   },
   {--17
           name='fs_used_perc',
           arg='/home',
           max=100,
           bg_colour=0xffffff,
           bg_alpha=0.2,
           fg_colour=0x2200cc,
           fg_alpha=0.8,
           x=380, y=300,
           radius=145,
           thickness=20,
           start_angle=220,
           end_angle=320
   },
}

clock_r=165
clock_x=824
clock_y=400

show_seconds=true

require 'cairo'

function rgb_to_r_g_b(colour,alpha)
    return ((colour / 0x10000) % 0x100) / 255., ((colour / 0x100) % 0x100) / 255., (colour % 0x100) / 255., alpha
end

function draw_ring(cr,t,pt)
    local w,h=conky_window.width,conky_window.height

    local xc,yc,ring_r,ring_w,sa,ea=pt['x'],pt['y'],pt['radius'],pt['thickness'],pt['start_angle'],pt['end_angle']
    local bgc, bga, fgc, fga=pt['bg_colour'], pt['bg_alpha'], pt['fg_colour'], pt['fg_alpha']

    local angle_0=sa*(2*math.pi/360)-math.pi/2
    local angle_f=ea*(2*math.pi/360)-math.pi/2
    local t_arc=t*(angle_f-angle_0)

    -- Draw background ring

    cairo_arc(cr,xc,yc,ring_r,angle_0,angle_f)
    cairo_set_source_rgba(cr,rgb_to_r_g_b(bgc,bga))
    cairo_set_line_width(cr,ring_w)
    cairo_stroke(cr)

    -- Draw indicator ring

    cairo_arc(cr,xc,yc,ring_r,angle_0,angle_0+t_arc)
    cairo_set_source_rgba(cr,rgb_to_r_g_b(fgc,fga))
    cairo_stroke(cr)
end

function draw_clock_hands(cr,xc,yc)
    local secs,mins,hours,secs_arc,mins_arc,hours_arc
    local xh,yh,xm,ym,xs,ys

    secs=os.date("%S")
    mins=os.date("%M")
    hours=os.date("%I")

    secs_arc=(2*math.pi/60)*secs
    mins_arc=(2*math.pi/60)*mins+secs_arc/60
    hours_arc=(2*math.pi/12)*hours+mins_arc/12

    -- Draw hour hand

    xh=xc+0.7*clock_r*math.sin(hours_arc)
    yh=yc-0.7*clock_r*math.cos(hours_arc)
    cairo_move_to(cr,xc,yc)
    cairo_line_to(cr,xh,yh)

    cairo_set_line_cap(cr,CAIRO_LINE_CAP_ROUND)
    cairo_set_line_width(cr,5)
    cairo_set_source_rgba(cr,1.0,1.0,1.0,1.0)
    cairo_stroke(cr)

    -- Draw minute hand

    xm=xc+clock_r*math.sin(mins_arc)
    ym=yc-clock_r*math.cos(mins_arc)
    cairo_move_to(cr,xc,yc)
    cairo_line_to(cr,xm,ym)

    cairo_set_line_width(cr,3)
    cairo_stroke(cr)

    -- Draw seconds hand

    if show_seconds then
        xs=xc+clock_r*math.sin(secs_arc)
        ys=yc-clock_r*math.cos(secs_arc)
        cairo_move_to(cr,xc,yc)
        cairo_line_to(cr,xs,ys)

        cairo_set_line_width(cr,1)
        cairo_stroke(cr)
    end
end

function conky_clock_rings()
    local function setup_rings(cr,pt)
        local str=''
        local value=0

        str=string.format('${%s %s}',pt['name'],pt['arg'])
        str=conky_parse(str)

        value=tonumber(str)
        pct=value/pt['max']
	if value == nil then value = 0
         end
        draw_ring(cr,pct,pt)
    end


    -- Check that Conky has been running for at least 5s

    if conky_window==nil then return end
    local cs=cairo_xlib_surface_create(conky_window.display,conky_window.drawable,conky_window.visual, conky_window.width,conky_window.height)

    local cr=cairo_create(cs)

    local updates=conky_parse('${updates}')
    update_num=tonumber(updates)

    if update_num>5 then
        for i in pairs(settings_table) do
            setup_rings(cr,settings_table[i])
        end
    end
    --[[ dynamic colors
    local procspee=conky_parse('${cpu1}')
    procspeed=tonumber(procspee)
    if procspeed >= 60 then
        settings_table[10]['fg_colour']=0xff0022
    else
        settings_table[10]['fg_colour']=0x66CC66
    end

    local procspee2=conky_parse('${cpu2}')
    procspeed2=tonumber(procspee2)
    if procspeed2 > 60 then
        settings_table[11]['fg_colour']=0xff0023
    else
        settings_table[11]['fg_colour']=0x66CC01
    end
 --]]

    draw_clock_hands(cr,clock_x,clock_y)
end
