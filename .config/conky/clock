-- vim: ts=4 sw=4 noet ai cindent syntax=lua
--[[ Conky configuration file ]]

conky.config = {
    background             = true,
    update_interval        = 1,
    double_buffer          = true,
    no_buffers             = true,
    imlib_cache_size       = 10,
    -- window specifications
    gap_x                  = 20,
    gap_y                  = 20,
    minimum_width          = 800,
	minimum_height         = 400,
    maximum_width          = 600,
    alignment              = 'top_right',
    own_window             = true,
    own_window_class       = 'Conky',
    own_window_type        = 'override',
    own_window_transparent = true,
    own_window_argb_visual = true,
    own_window_argb_value  = 255,
    own_window_hints       = 'undecorated,sticky,skip_taskbar,skip_pager,below',
    border_inner_margin    = 0,
    border_outer_margin    = 0,
    -- graphics settings
    draw_shades            = false,
    default_shade_color    = '282828',
    draw_outline           = false,
    default_outline_color  = '282828',
    draw_borders           = false,
    draw_graph_borders     = false,
    --default_graph_size=26 80,
    show_graph_scale       = false,
    show_graph_range       = false,
    -- text settings
    use_xft                = true,
    xftalpha               = 0,
    font                   = 'Inconsolata Nerd Font:size=10',
    text_buffer_size       = 256,
    override_utf8_locale   = true,
    -- useful bits
    short_units            = true,
    pad_percents           = 2,
    top_name_width         = 30,
    -- color scheme
    default_color          = '282828',
    --color                  = '282828',
    color1                 = 'cc241d',
    color2                 = '98971a',
    color3                 = 'd79921',
    color4                 = '458588',
    color5                 = 'b16286',
    color6                 = '689d6a',
    color7                 = 'a89984',
    color8                 = '928374',
    color9                 = 'fb4934',
	--color10                = 'b8bb26',
	--color11                = 'fabd2f',
	--color12                = '83a598',
	--color13                = 'd3869b',
	--color14                = '8ec07c',
	--color15                = 'ebdbb2',

	-- lua scripts
	--lua_load			   = '~/.conky/scripts/clock.lua',
	--lua_draw_hook_pre	   = 'draw_clock',
	lua_load			   = '~/.conky/scripts/clock_rings.lua',
	lua_draw_hook_pre	   = 'clock_rings',
};

conky.text = [[
]];

--[[
conky.text = [[
	${voffset 20}
	${goto 40}${cpugraph 26,190 FFFFFF FFFFFF -l}
	${goto 40}Cpu:${alignr 39}${cpu}%

	${goto 40}${memgraph 26,190 FFFFFF FFFFFF -l}
	${goto 40}Mem:${alignr 39}${memperc}%

	${goto 40}${diskiograph 26,190 FFFFFF FFFFFF -l}
	${goto 40}Disk I/O:${alignr 39}${diskio}
	${hr 2}

	TOP PROCESSES ${hr 2} Name $alignr PID CPU% MEM% ${top name 1} $alignr ${top pid 1} ${top cpu 1}% ${top mem 1}% ${top name 2} $alignr ${top pid 2} ${top cpu 2}% ${top mem 2}% ${top name 3} $alignr ${top pid 3} ${top cpu 3}% ${top mem 3}% ${top name 4} $alignr ${top pid 4} ${top cpu 4}% ${top mem 4}% ${top name 5} $alignr ${top pid 5} ${top cpu 5}% ${top mem 5}% ${top name 6} $alignr ${top pid 6} ${top cpu 6}% ${top mem 6}% ${top name 7} $alignr ${top pid 7} ${top cpu 7}% ${top mem 7}%
]]--;

--[[
${execi 300 curl -s "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D28350087&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys" -o ~/.cache/weather.xml}
\
# Weather
\
${goto 36}${voffset 0}${font Hack Nerd Font:size=36}${color1}${execi 300 grep "yweather:condition" ~/.cache/weather.xml | grep -o "temp=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"}°${font}${color}
# I had to comment out this line - was printing out and overlaying.
#${goto 46}${voffset 14}${font Hack Nerd Font:size=12}${color1}${execi 300 grep "yweather:condition" ~/.cache/weather.xml | grep -o "text=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"}${font}${color}
\
${color1}${alignr 55}${voffset -72}${execi 300 grep "yweather:atmosphere" ~/.cache/weather.xml | grep -o "pressure=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"} ${execi 300 grep "yweather:units" ~/.cache/weather.xml | grep -o "pressure=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"}
${color1}${alignr 55}${voffset 7}${execi 300 grep "yweather:atmosphere" ~/.cache/weather.xml | grep -o "humidity=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"} %${color}
${color1}${alignr 55}${voffset 7}${execi 300 grep "yweather:wind" ~/.cache/weather.xml | grep -o "speed=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"} ${execi 300 grep "yweather:units" ~/.cache/weather.xml | grep -o "speed=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*"}${color}
\
${color3}${voffset 30}${alignc 77}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "day=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==1' | tr '[a-z]' '[A-Z]'}${color}
${color3}${voffset -13}${alignc}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "day=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==2' | tr '[a-z]' '[A-Z]'}${color}
${color3}${voffset -13}${alignc -77}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "day=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==3' | tr '[a-z]' '[A-Z]'}${color}
\
${color2}${voffset 40}${alignc 77}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "low=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==1'}°/${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "high=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==1'}°${color}
${color2}${voffset -13}${alignc}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "low=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==2'}°/${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "high=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==2'}°${color}
${color2}${voffset -13}${alignc -77}${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "low=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==3'}°/${execi 300 grep "yweather:forecast" ~/.cache/weather.xml | grep -o "high=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==3'}°${color}
${hr 2}
\
# Cpu, memory and disk IO
\
${voffset 20}
${goto 40}${cpugraph 26,190 FFFFFF FFFFFF -l}
${goto 40}Cpu:${alignr 39}${cpu}%

${goto 40}${memgraph 26,190 FFFFFF FFFFFF -l}
${goto 40}Mem:${alignr 39}${memperc}%

${goto 40}${diskiograph 26,190 FFFFFF FFFFFF -l}
${goto 40}Disk I/O:${alignr 39}${diskio}
${hr 2}
\
# Network
\
${if_existing /proc/net/route wlp59s0}
${goto 40}${upspeedgraph wlp59s0 26,80 FFFFFF FFFFFF}${goto 150}${downspeedgraph wlp59s0 26,80 FFFFFF FFFFFF}
${goto 40}${color5}Up: ${color2}${upspeed wlp59s0}${color5}${goto 150}Down: ${color2}${downspeed wlp59s0}
${goto 40}${color5}Sent: ${color2}${totalup wlp59s0}${color5}${goto 150}Received: ${color2}${totaldown wlp59s0}
${endif}
${if_existing /proc/net/route ens20u1u2}
${goto 40}${upspeedgraph ens20u1u2 26,80 FFFFFF FFFFFF}${goto 150}${downspeedgraph ens20u1u2 26,80 FFFFFF FFFFFF}
${goto 40}${color5}Up: ${color2}${upspeed ens20u1u2}${color5}${goto 150}Down: ${color2}${downspeed ens20u1u2}
${goto 40}${color5}Sent: ${color2}${totalup ens20u1u2}${color5}${goto 150}Received: ${color2}${totaldown ens20u1u2}
${endif}
${hr 2}
\
# Proceses
\
${goto 40}${color}Proc${color}${alignr 39}${color}${color}Mem${color}
${goto 40}${color2}${top_mem name 1}${color5}${alignr 39}${top_mem mem_res 1}
${goto 40}${color2}${top_mem name 2}${color5}${alignr 39}${top_mem mem_res 2}
${goto 40}${color2}${top_mem name 3}${color5}${alignr 39}${top_mem mem_res 3}
${goto 40}${color2}${top_mem name 4}${color5}${alignr 39}${top_mem mem_res 4}
${goto 40}${color2}${top_mem name 5}${color5}${alignr 39}${top_mem mem_res 5}

${goto 40}${color1}Proc${color}${alignr 39}${color1}Cpu %${color}
${goto 40}${color2}${top_mem name 1}${color2}${alignr 39}${top cpu 1} %
${goto 40}${color2}${top_mem name 2}${color2}${alignr 39}${top cpu 2} %
${goto 40}${color2}${top_mem name 3}${color2}${alignr 39}${top cpu 3} %
${goto 40}${color2}${top_mem name 4}${color2}${alignr 39}${top cpu 4} %
${goto 40}${color2}${top_mem name 5}${color2}${alignr 39}${top cpu 5} %
${hr 2}
\
# System shortcuts
${font Hack Nerd Font:size=10}
${goto 40}W-t${alignr 40}Terminal
${goto 40}W-e${alignr 40}Editor
${goto 40}W-f${alignr 40}File browser
${goto 40}W-w${alignr 40}Web browser
${goto 40}C-W-F4${alignr 40}Log off
\
# Various images including the icons of the forecast #
\
${image ~/.conky/images/pressure.png -p 214,20 -s 16x16}\
${image ~/.conky/images/humidity.png -p 214,40 -s 16x16}\
${image ~/.conky/images/wind.png -p 214,60 -s 16x16}\
${execi 300 cp -f ~/.conky/icons/$(grep "yweather:forecast" ~/.cache/weather.xml | grep -o "code=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==1').png ~/.cache/weather-1.png}${image ~/.cache/weather-1.png -p 41,120 -s 32x32}\
${execi 300 cp -f ~/.conky/icons/$(grep "yweather:forecast" ~/.cache/weather.xml | grep -o "code=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==2').png ~/.cache/weather-2.png}${image ~/.cache/weather-2.png -p 119,120 -s 32x32}\
${execi 300 cp -f ~/.conky/icons/$(grep "yweather:forecast" ~/.cache/weather.xml | grep -o "code=\"[^\"]*\"" | grep -o "\"[^\"]*\"" | grep -o "[^\"]*" | awk 'NR==3').png ~/.cache/weather-3.png}${image ~/.cache/weather-3.png -p 195,120 -s 32x32}${font}\
]]
