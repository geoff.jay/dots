# Setup fzf
# ---------
if [[ ! "$PATH" == */home/crdc/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/crdc/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/crdc/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/crdc/.fzf/shell/key-bindings.bash"
