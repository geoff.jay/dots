# Dotfiles

These are my dotfiles, there are many like them, these are mine.

## Dependencies

- `neovim`
- `neofetch`
- `powerline-shell`
- `fzf` from https://github.com/junegunn/fzf

## Setup

Non-exhaustive setup steps. This assumes a fresh installation, use the
`rsync --delete` at your own peril.

```sh
cd ~
git clone https://gitlab.com/geoff.jay/dots.git .dots
rsync -av --delete --exclude=.dots --exclude=.ssh \
  --exclude=.local --exclude=.cache --exclude=.git \
  .dots/ $HOME/
```

## Update

Probably not the best idea to include `--delete` after there's been local
configuration.

```sh
cd ~/.dots
git pull
cd ~
rsync -av --exclude=./README.md --exclude=.git .dots/ $HOME/
```
